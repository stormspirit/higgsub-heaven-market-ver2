package com.heaven.market.enums;

/**
 * Created by son on 27/04/2016.
 */
public enum  StatusProduct {
    SOLD, // da ban (for only student)
    SELLING, // DANG BAN (for only student)
    BOUGHT // da mua (for both student and register)
}
