package com.heaven.market.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by Storm on 27/04/2016.
 */
public class InterceptorException extends Exception{
    private HttpStatus httpStatus;

    public InterceptorException() {
    }

    public InterceptorException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
