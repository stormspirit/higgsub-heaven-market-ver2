package com.heaven.market.model;

import com.heaven.market.controller.dto.ProductDTO;
import com.heaven.market.enums.StatusProduct;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Administrator on 27/04/2016.
 */
@Entity
@Table (name = "PRODUCT")
public class Product {
    @Id
    @GeneratedValue
    @Column(unique = true, nullable = false)
    private int productID;

    @ManyToMany
    @JoinTable(name = "product_category",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns  = @JoinColumn(name = "category_id"))
    private List<Category> categoryList;

    @OneToMany
    private List<Comment> commentList;

    private String nameProductVN;
    private String nameProductENG;

    @Enumerated
    private StatusProduct status;

    private long priceProduct;
    private String contentIntro;
    // nghien cuu va bo sung file media/jpge sau

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getNameProductVN() {
        return nameProductVN;
    }

    public void setNameProductVN(String nameProductVN) {
        this.nameProductVN = nameProductVN;
    }

    public String getNameProductENG() {
        return nameProductENG;
    }

    public void setNameProductENG(String nameProductENG) {
        this.nameProductENG = nameProductENG;
    }

    public String getContentIntro() {
        return contentIntro;
    }

    public void setContentIntro(String contentIntro) {
        this.contentIntro = contentIntro;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public StatusProduct getStatus() {
        return status;
    }

    public void setStatus(StatusProduct status) {
        this.status = status;
    }

    public long getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(long priceProduct) {
        this.priceProduct = priceProduct;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }
}
