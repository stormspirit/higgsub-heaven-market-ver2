package com.heaven.market.model;

import com.heaven.market.controller.dto.UserDTO;
import com.heaven.market.enums.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 27/04/2016.
 */
@Entity
@Table (name = "USER")
public class User {
    @Id
    private String token;

    @Column(unique = true, nullable = false)
    private String username;

    private String password;
    private String email;
    private Date expireDate;
    private float trustPoint;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany
    private List<Product> productList = new ArrayList<Product>();

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }


    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public float getTrustPoint() {
        return trustPoint;
    }

    public void setTrustPoint(float trustPoint) {
        this.trustPoint = trustPoint;
    }
}
