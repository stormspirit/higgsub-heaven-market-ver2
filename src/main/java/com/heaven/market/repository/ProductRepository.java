package com.heaven.market.repository;

import com.heaven.market.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 27/04/2016.
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Integer>{
}
