package com.heaven.market.controller;

import com.heaven.market.controller.stereotype.NoAuthentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 27/04/2016.
 */
@RestController
public class TestController {
    @NoAuthentication
    @RequestMapping(value = "/shown")
    public String showInfo(){
        return "111111111111111";
    }
}
