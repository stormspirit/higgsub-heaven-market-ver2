package com.heaven.market.controller;

import com.heaven.market.controller.dto.ProductDTO;
import com.heaven.market.controller.stereotype.NoAuthentication;
import com.heaven.market.controller.stereotype.RequiredRoles;
import com.heaven.market.enums.Role;
import com.heaven.market.model.Comment;
import com.heaven.market.model.Product;
import com.heaven.market.service.ProductService;
import com.heaven.market.SearchEngine.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 27/04/2016.
 */
@RestController
public class ProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private SearchService searchService;

    @RequiredRoles({Role.STUDENT, Role.REGISTER})
    @RequestMapping(value = "/{username}/products/createProduct", method = RequestMethod.POST)
    public List<ProductDTO> addProduct (@PathVariable("username") String username, @RequestBody  ProductDTO productDTO){
        return productService.addProduct(username, productDTO);
    }

    @RequiredRoles({Role.STUDENT, Role.REGISTER})
    @RequestMapping(value = "/{username}/products", method = RequestMethod.GET)
    public List<ProductDTO> findProduct(@PathVariable("username") String username){
        return productService.showProducts(username);
    }

    @RequiredRoles({Role.STUDENT, Role.REGISTER})
    @RequestMapping(value = "/{username}/products/{productID}/delete", method = RequestMethod.GET)
    public boolean deleteProduct(@PathVariable("username") String username, @PathVariable("productID") int productID) {
        return productService.deleteProduct(username, productID);
    }

    @NoAuthentication
    @RequestMapping(value = "/search/{query}", method = RequestMethod.GET)
    public List<ProductDTO> searchByName(@PathVariable("query") String query){
        return searchService.searchByName(query);
    }

    @NoAuthentication
    @RequestMapping(value = "/Categories/search/{query}", method = RequestMethod.GET)
    public List<ProductDTO> searchByCategory(@PathVariable("query") String query){
        return searchService.searchByCategory(query);
    }
}
