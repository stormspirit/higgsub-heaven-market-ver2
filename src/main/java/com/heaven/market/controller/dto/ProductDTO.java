package com.heaven.market.controller.dto;

import com.heaven.market.enums.StatusProduct;
import com.heaven.market.model.Category;
import com.heaven.market.model.Product;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 27/04/2016.
 */
public class ProductDTO {
    private int productID;
    private String nameProductVN;
    private StatusProduct statusProduct;
//    private String imageProduct;
    private long priceProduct;
    private String contentIntro;
    private List<String> categoryList;

    public String getNameProductVN() {
        return nameProductVN;
    }

    public void setNameProductVN(String nameProductVN) {
        this.nameProductVN = nameProductVN;
    }
//
//    public String getImageProduct() {
//        return imageProduct;
//    }
//
//    public void setImageProduct(String imageProduct) {
//        this.imageProduct = imageProduct;
//    }
//
    public ProductDTO(Product product) {
        this.productID = product.getProductID();
        this.nameProductVN = product.getNameProductVN();
        this.contentIntro = product.getContentIntro();

        List<String> categoryNameList = new ArrayList<String>();
        for (Category category : product.getCategoryList()) {
            categoryNameList.add(category.getNameCategory());
        }
        this.categoryList = categoryNameList;

        this.priceProduct = product.getPriceProduct();
        this.statusProduct = product.getStatus();
    }

    public String getContentIntro() {
        return contentIntro;
    }

    public void setContentIntro(String contentIntro) {
        this.contentIntro = contentIntro;
    }

    public long getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(long priceProduct) {
        this.priceProduct = priceProduct;
    }

    public StatusProduct getStatusProduct() {
        return statusProduct;
    }

    public void setStatusProduct(StatusProduct statusProduct) {
        this.statusProduct = statusProduct;
    }

    public List<String> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<String> categoryList) {
        this.categoryList = categoryList;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }
}
