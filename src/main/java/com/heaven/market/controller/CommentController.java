package com.heaven.market.controller;

import com.heaven.market.controller.stereotype.RequiredRoles;
import com.heaven.market.enums.Role;
import com.heaven.market.model.Comment;
import com.heaven.market.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by son on 30/04/2016.
 */
@RestController
public class CommentController {
    @Autowired
    private ProductService productService;

    @RequiredRoles({Role.ADMIN, Role.REGISTER, Role.STUDENT})
    @RequestMapping(value = "/products/{productID}/comments/addOne", method = RequestMethod.POST)
    public List<Comment> addComment(@PathVariable("productID") int productID, @RequestBody Comment comment) {
        return productService.addComment(productID, comment);
    }

    @RequiredRoles({Role.ADMIN, Role.REGISTER, Role.STUDENT})
    @RequestMapping(value = "/products/{productID}/comments/deleteOne", method = RequestMethod.POST)
    public List<Comment> deleteComment(@PathVariable("productID") int productID, @RequestBody int commentID) {
        return productService.deleteComment(productID, commentID);
    }
}
