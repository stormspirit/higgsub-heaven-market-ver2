package com.heaven.market.SearchEngine;

import com.heaven.market.model.Product;
import com.heaven.market.repository.ProductRepository;
import com.heaven.market.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by son on 29/04/2016.
 */
@Service
public class SearchFunctions {
    @Autowired
    private InvertedIndexRepository invertedIndexRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductService productService;
    // update bang Inverted Index
    public void updateInverted() {
        List<Product> products = (List<Product>) productRepository.findAll();
        for (Product product : products) {
            productService.createInvertedIndex(product);
        }
    }
    // Ham chuyen tu String "1,2,3,5,6," => {1, 2, 3, 5, 6}
    private static List<Integer> convertStringToIntList(String s) {
        List<Integer> intList = new ArrayList<Integer>();
        s = s.substring(0, s.length() - 1);
        String[] words = s.split(",");
        for (String word : words) {
            intList.add(Integer.parseInt(word));
        }
        return intList;
    }

    public String fixQuery(String query) {
        return unAccent(fixSpacesString(query));
    }
    // remove mark of Vietnamese word.
    private static String unAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll("đ", "d");
    }

    // Xu ly dau cach' trong string
    private static String fixSpacesString(String s) {
        return s.trim().replaceAll("\\s+", " ");
    }
    /*
    * Ham tim giao cua cac tap hop
    * */
    public List<Integer> intersection(List<InvertedIndex> keyList) {
        List<Integer> result = new ArrayList<Integer>();

        for (InvertedIndex key : keyList) {
            List<Integer> temp = convertStringToIntList(key.getSequence());
            if (result.size() == 0) result.addAll(temp);
            result.retainAll(temp);
        }
        return result;
    }


}
