package com.heaven.market.SearchEngine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by son on 24/04/2016.
 * <p>
 * Gioi thieu ve phuong phap tim kiem Inverted Index
 * Gia su ta co 3 products P1, P2, P3 lan luot co ten la`:
 * <p>
 * P1.proName = "san pham thu nhat",
 * P2.proName = "san pham so hai",
 * P3.proName = "product so ba",
 * <p>
 * Inverted Index co':
 * "san": "1,2"
 * "pham": "1,2"
 * "thu" : "1"
 * "so" : "2,3"
 * "nhat" : "1"
 * "hai" : "2"
 * "ba" : "3"
 * "product" : "3"
 * <p>
 * Gia su query = "san pham hai" => {1,2} * {1,2} * {2} = {2}
 */

@Entity
@Table(name = "invertedIndex")
public class InvertedIndex {
    @Id
    @Column(unique = true)
    private String word; // Tu khoa tim kiem
    private String sequence; // Chuoi~ chua productID vd: 1,2,3,5,6,

    public InvertedIndex() {
        word = "";
        sequence = "";
    }
    // Constructor them ma san pham
    public InvertedIndex(String word, int productID) {
        this.word = word;
        this.sequence = productID + ",";
    }

    // Them ma san pham vao duoi
    public void addSequence(int productID) {
        this.sequence = this.sequence + productID + ",";
    }

    // Xoa 1 san pham di
    public void deleteProductID(int productID) {
        String productIDstring = productID + ",";
        this.sequence = this.sequence.replaceAll(productIDstring, "");
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getSequence() {
        return sequence;
    }
}
